import React from "react";
import logo from "../assets/logo.png";
import menu from "../assets/menu.png";
import deletePng from "../assets/delete.png";
import { useState } from "react";

const Nav = () => {
  const [showBtn, setShowBtn] = useState(false);
  return (
    <nav className="bg-[#222]">
      <div className="container">
        <div className="flex justify-between items-center	">
          <a href="">
            <img src={logo} alt="" />
          </a>
          <ul className="hidden md:flex text-white gap-14">
            <a href="">
              <li>Home</li>
            </a>
            <a href="#projects">
              <li>Projects</li>
            </a>
            <a href="#experience">
              <li>Experience</li>
            </a>
            <a href="#contact">
              <li>Contact</li>
            </a>
          </ul>
          <button onClick={()=>{setShowBtn(true)}} className="md:hidden">
            <img src={menu} alt="" className="h-8" />
          </button>
        </div>
        {
          showBtn && <div className="navigation fixed h-screen top-0 left-0 bg-[#222] w-[80%]">
          <div className="flex justify-end">
          <button onClick={()=>{setShowBtn(false)}} className="p-4"><img className="w-8" src={deletePng} alt="" /></button>
          </div>
          <ul className=" text-white flex flex-col">
            <a className="w-full pl-4 py-3 hover:bg-[#3a3a3a]" href="">
              <li>Home</li>
            </a>
            <a className="w-full pl-4 py-3 hover:bg-[#3a3a3a]" href="#projects">
              <li>Projects</li>
            </a>
            <a className="w-full pl-4 py-3 hover:bg-[#3a3a3a]" href="#experience">
              <li>Experience</li>
            </a>
            <a className="w-full pl-4 py-3 hover:bg-[#3a3a3a]" href="#contact">
              <li>Contact</li>
            </a>
          </ul>
        </div>

        }
        
      </div>
    </nav>
  );
};

export default Nav;
