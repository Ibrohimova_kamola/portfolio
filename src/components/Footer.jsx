import React from "react";
import "./Footer.css"
import instagram from "../assets/instag.png";
import email from "../assets/email.png";
import youtube from "../assets/youtube.png";
import x from "../assets/x.png";

const Footer = () => {
  return (
    <footer className="bg-[#222] py-24 footer">
      <div className="container">
        <div className="">
          <h2 id="contact" className="text-3xl text-white font-bold mb-8">Contact</h2>
          <p className="text-gray-400 max-w-[740px] text-lg">
            Seasoned Full Stack Software Engineer with over 8 years of hands-on
            experience in designing and implementing robust, scalable, and
            innovative web solutions. Adept at leveraging a comprehensive skill
            set encompassing front-end and back-end technologies{" "}
          </p>
          <span className="flex items-center gap-2 text-white font-bold my-7"   >
            <img src={email} alt="" /> abmcodehub@gmail.com
          </span>
          <div className=" flex items-center gap-5">
            <img src={instagram} alt="" />
            <img src={x} alt="" />
            <img src={youtube} alt="" />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
