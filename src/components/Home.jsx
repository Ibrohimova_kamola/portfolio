import React from "react";
import "./Home.css"
import profile from "../assets/profile.png";
import javascript from "../assets/javascript.png";
import nodejs from "../assets/nodejs.png";
import html from "../assets/html.png";
import css from "../assets/css.png";
import reactjs from "../assets/reactjs.png";
import htmlp from "../assets/htmlp.png";
import cssp from "../assets/cssp.png";
import victor from "../assets/victor.png";
import google from "../assets/google-logo.png";
import apple from "../assets/apple-logo.png";
import meta from "../assets/meta-logo.png";

const Home = () => {
  return (
    <div className="bg-zinc-950">
      <div className="container">
        <div className="hero_section flex flex-col items-center py-20">
          <div className="bg-gradient-to-r from-purple-500 to-pink-500 rounded-full p-4">
            <img className="w-56" src={profile} alt="" />
          </div>

          <h1 className="text-white text-center text-5xl font-bold max-w-[670px] my-10">
            Hi, I'm{" "}
            <span className="bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500 ">
              {" "}
              Kamola Ibrohimova,
            </span>{" "}
            Front-end Developer
          </h1>
          <p className="max-w-[680px] text-center text-gray-400 text-lg hero_text">
            I am a seasoned full-stack software engineer with over 8 years of
            professional experience, specializing in backend development. My
            expertise lies in crafting robust and scalable SaaS-based
            architectures on the Amazon AWS platform.
          </p>
          <div className="hero_btns flex gap-6 mt-12">
            <button className="py-3 px-8 font-bold text-xl rounded-full border-2 border-white bg-white">
              Get In Touch
            </button>
            <button className="py-3 px-8 font-bold text-xl rounded-full border-2 text-white ">
              Download CV
            </button>
          </div>
        </div>
        <main>
          <section className="flex flex-col items-center">
            <h2 className="text-gray-400 text-3xl font-bold tracking-widest hero_title">
              EXPERIENCE WITH
            </h2>
            <div className="flex items-center gap-16 mt-14 technolgy">
              <img src={javascript} alt="" />
              <img src={nodejs} alt="" />
              <img src={html} alt="" />
              <img src={css} alt="" />
              <img src={reactjs} alt="" />
            </div>
          </section>
          <section className="py-32 projects">
            <h2 id="projects" className="text-orange-500 text-center text-4xl font-bold mb-8 hero_title">
              Projects
            </h2>
            <div className="project_cards flex justify-center gap-9">
              <div className="">
                <img className="base_img" src={htmlp} alt="" />
                <div className="bg-[#343333] rounded-b-lg flex items-center justify-between p-4">
                  <div className="text-white font-bold">
                    <span className="text-xs text-zinc-400">
                      CLICK HERE TO VISIT
                    </span>
                    <h4 className="text-lg">HTML TUTORIAL</h4>
                  </div>
                  <img src={victor} alt="" />
                </div>
              </div>
              <div className="">
                <img  className="base_img" src={cssp} alt="" />
                <div className="bg-[#343333] rounded-b-lg flex items-center justify-between p-4">
                  <div className="text-white font-bold">
                    <span className="text-xs text-zinc-400">
                      CLICK HERE TO VISIT
                    </span>
                    <h4 className="text-lg">HTML TUTORIAL</h4>
                  </div>
                  <img src={victor} alt="" />
                </div>
              </div>
            </div>
          </section>
          <section>
            <div className="pb-24 experience">
              <h2 id="experience" className="text-blue-500 text-center text-4xl font-bold mb-12 hero_title">
                Experience
              </h2>
              <div className="flex flex-col gap-10">
                <div className="">
                  <div className="flex justify-between experience_row">
                    <h4 className="text-white flex items-center text-2xl font-bold gap-4">
                      <img src={google} alt="" />
                      Lead Software Engineer at Google
                    </h4>
                    <span className="text-gray-400">Nov 2019 - Present</span>
                  </div>
                  <p className="text-gray-400 text-lg mt-6">
                    As a Senior Software Engineer at Google, I played a pivotal
                    role in developing innovative solutions for Google's core
                    search algorithms. Collaborating with a dynamic team of
                    engineers, I contributed to the enhancement of search
                    accuracy and efficiency, optimizing user experiences for
                    millions of users worldwide.
                  </p>
                </div>
                <div className="">
                  <div className="flex justify-between experience_row">
                    <h4 className="text-white flex items-center text-2xl font-bold gap-4">
                      <img src={apple} alt="" />
                      Junior Software Engineer at Apple
                    </h4>
                    <span className="text-gray-400">Nov 2019 - Present</span>
                  </div>
                  <p className="text-gray-400 text-lg mt-6">
                  During my tenure at Apple, I held the role of Software Architect, where I played a key role in shaping the architecture of mission-critical software projects. Responsible for designing scalable and efficient systems, I provided technical leadership to a cross-functional team.
                  </p>
                </div>
                <div className="">
                  <div className="flex justify-between experience_row">
                    <h4 className="text-white flex items-center text-2xl font-bold gap-4">
                      <img src={meta} alt="" />
                      Software Engineer at Meta
                    </h4>
                    <span className="text-gray-400">Nov 2019 - Present</span>
                  </div>
                  <p className="text-gray-400 text-lg mt-6">
                  At Meta, I served as a  Software Engineer, focusing on the design and implementation of backend systems for the social media giant's dynamic platform. Working on projects that involved large-scale data processing and user engagement features, I leveraged my expertise to ensure seamless functionality and scalability.
                  </p>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default Home;
